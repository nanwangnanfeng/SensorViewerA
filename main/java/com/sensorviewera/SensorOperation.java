package com.sensorviewera;

import android.hardware.Sensor;
import android.hardware.SensorManager;

import android.os.Handler;
public class SensorOperation
{
    public boolean isTrans; 
    public GroupItem item; 
    private static SensorManager sm=null;
    private static Handler sender=null;
    private Sensor sensor; 
    private String data_unit;
    private SensorData sensor_data;
    private SensorListener sensor_listener;
    public static void init(SensorManager sm_,Handler h)
    {
        sm=sm_;
        sender=h;
    }
    public SensorOperation(Sensor s, String head_name,GroupItem item_,String data_unit_)
    {
        sensor=s;
        sensor_data=new SensorData(head_name);
        sensor_listener=new SensorListener(sensor_data,sender);
        item=item_;
        isTrans=false;
        data_unit=data_unit_;
        if(sensor.getMinDelay()>80000||sensor.getMinDelay()==0)
            sensor_data.setFeq(0);
    }
    public void setSamplingFre(int feq)
    {
        if(sensor.getMinDelay()<80000&&sensor.getMinDelay()!=0)
            sensor_data.setFeq(feq);
    }
    public void startTrans() 
    {
        sensor_data.reset();
        sensor_listener.setEnable(true);
        startSensor();
        isTrans=true;
    }
    public void startSensor()
    {
        int v;
        if(sensor.getMinDelay()>80000||sensor.getMinDelay()==0)
            v=SensorManager.SENSOR_DELAY_FASTEST;
        else
            v=SensorManager.SENSOR_DELAY_GAME;
        sm.registerListener(sensor_listener,sensor,v);
    }
    public void stopTrans()
    {
        sm.unregisterListener(sensor_listener);//关闭传感器
        //sensor_listener.setEnable(false);
        isTrans=false;
    }
    public String getMaxRange(){return String.valueOf(sensor.getMaximumRange())+data_unit;}
    public String getMinDelay(){return String.valueOf(sensor.getMinDelay())+"ms";}
    public String getName(){return sensor.getName();}
    public String getPower(){return String.valueOf(sensor.getPower())+"mA";}
    public String getResolution(){return String.valueOf(sensor.getResolution())+data_unit;}
    public String getVendor(){return sensor.getVendor();}
    public String getVersion(){return String.valueOf(sensor.getVersion());}

}
