package com.sensorviewera;

import android.view.ContextMenu;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;

public class SendingSensorList extends SensorList
{
    public SendingSensorList(ExpandableListView l)
    {
        super(l);
        setListContextMenu();
        setItemLongClicked();
        setChildClicked();
    }
    public SendingSensorList(ExpandableListView l,ArrayList<GroupItem> groups)
    {
        super(l,groups);
        setListContextMenu();
        setItemLongClicked();
        setChildClicked();
    }
    private void setListContextMenu()
    {
        list.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener()
        {
            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo)
            {
                SensorViewerAMainActivity a=(SensorViewerAMainActivity)list.getContext();
                contextMenu.setHeaderTitle(a.getString(R.string.context_menu_title));
                contextMenu.add(0, a.CONTEXT_MENU_5Hz, 0, a.getString(R.string.feq5hz));
                contextMenu.add(0, a.CONTEXT_MENU_10Hz, 0, a.getString(R.string.feq10hz));
                contextMenu.add(0, a.CONTEXT_MENU_25Hz, 0, a.getString(R.string.feq25hz));
                contextMenu.add(0, a.CONTEXT_MENU_50Hz, 0, a.getString(R.string.feq50hz));
            }
        });
    }
}
